<?php
include 'session_functions.php';
require 'db_conn.php';

get_guest_info('guests_data');

$tableName = 'participants';
$sqlCreateTable = "CREATE TABLE IF NOT EXISTS $tableName (
  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  topic VARCHAR(255) NOT NULL,
  payment_method VARCHAR(255) NOT NULL,
  subscription VARCHAR(3) NOT NULL,
  date DATETIME NOT NULL,
  ip VARCHAR(255) NOT NULL,
  status VARCHAR(20) NOT NULL
)";

$pdo->exec($sqlCreateTable);

if (!empty($_POST)) {
    $name = $_POST['name'] ?? '';
    $surname = $_POST['surname'] ?? '';
    $email = $_POST['email'] ?? '';
    $phone = $_POST['phone'] ?? '';
    $topic = $_POST['topic'] ?? '';
    $payment = $_POST['payment_method'] ?? '';
    $subscribe = isset($_POST['subscribe']) ? 'yes' : 'no';
    $date = date('Y-m-d H:i:s') ?? 0;
    $ip = $_SERVER['REMOTE_ADDR'] ?? '';

    $errors = [];

    $data = [
        'name' => strip_tags($name),
        'surname' => strip_tags($surname),
        'email' => strip_tags($email),
        'phone' => strip_tags($phone),
        'topic' => strip_tags($topic),
        'payment' => strip_tags($payment),
        'subscription' => strip_tags($subscribe),
        'date' => $date,
        'ip' => $ip,
        'status' => 'active'
    ];

    foreach ($data as $key => $value) {
        if (empty($value)) {
            $errors[$key] = "empty";
        }
    }

    if (!empty($errors)) {
        include 'templates/form2_fixed.php';
    } else {
        $stmt = $pdo->prepare("INSERT INTO $tableName (name, surname, email, phone, topic, payment_method, subscription, date, ip, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->execute([$data['name'], $data['surname'], $data['email'], $data['phone'], $data['topic'], $data['payment'], $data['subscription'], $data['date'], $data['ip'], $data['status']]);
        echo '<span style="color: green;">Форма успешно отправлена</span>';
        include 'templates/form2_fixed.php';
        unset($_POST);
    }

} else {
    include 'templates/form2_fixed.php';
}
?>
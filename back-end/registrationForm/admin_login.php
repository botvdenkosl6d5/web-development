<?php
session_start();

$username = $_POST['username'] ?? '';
$password = $_POST['password'] ?? '';

$hashed_password = md5('useruser');

if (isset($_SESSION['authentification'])) {
    header('Location: admin_new.php');
}

if (!empty($username) && !empty($password)) {
    if ($username === 'user' && md5($password) === $hashed_password) {
        $_SESSION['authentification'] = true;
        header('Location: admin_new.php');

        $_SESSION['last_activity'] = time();
    }
    else {
        $error = 'Incorrect username or password!';
    }
}

include 'templates/login_form.html';
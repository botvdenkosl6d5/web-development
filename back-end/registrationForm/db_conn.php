<?php

$host = '127.0.01';
$username = 'db_form_test60';
$password = '6uNAxn8P';

$conn = "mysql:host=$host;dbname=$username";
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
try {
    $pdo = new PDO($conn, $username, $password, $options);
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
}
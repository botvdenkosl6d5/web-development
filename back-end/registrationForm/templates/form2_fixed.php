<h1>Заявка на участие в конференции</h1>

<form method="POST" action="">
  <label for="name">Имя:</label>
  <input type="text" id="name" name="name" value="<?= htmlspecialchars($_POST['name'] ?? '') ?>">
  <?= isset($errors['name']) && $errors['name'] == 'empty' ? '<span style="color: red;">Не указано имя</span>' : 
  (isset($errors['name']) && $errors['name'] == 'unnecessary divider detected' ? '<span style="color: red;">Содержит запрещенный символ</span>' : '') ?>
  <br><br>

  <label for="surname">Фамилия:</label>
  <input type="text" id="surname" name="surname" value="<?= htmlspecialchars($_POST['surname'] ?? '') ?>">
  <?= isset($errors['surname']) && $errors['surname'] == 'empty' ? '<span style="color: red;">Не указана фамилия</span>' : 
  (isset($errors['surname']) && $errors['surname'] == 'unnecessary divider detected' ? '<span style="color: red;">Содержит запрещенный символ</span>' : '') ?>
  <br><br>

  <label for="email">E-mail:</label>
  <input type="email" id="email" name="email" value="<?= htmlspecialchars($_POST['email'] ?? '') ?>">
  <?= isset($errors['email']) && $errors['email'] == 'empty' ? '<span style="color: red;">Не указана почта</span>' : 
  (isset($errors['email']) && $errors['email'] == 'unnecessary divider detected' ? '<span style="color: red;">Содержит запрещенный символ</span>' : '') ?>
  <br><br>

  <label for="phone">Телефон:</label>
  <input type="tel" id="phone" name="phone" value="<?= htmlspecialchars($_POST['phone'] ?? '') ?>">
  <?= isset($errors['phone']) && $errors['phone'] == 'empty' ? '<span style="color: red;">Не указан номер телефона</span>' : 
  (isset($errors['phone']) && $errors['phone'] == 'unnecessary divider detected' ? '<span style="color: red;">Содержит запрещенный символ</span>' : '') ?>
  <br><br>

  <label for="topic">Тематика конференции:</label>
  <select id="topic" name="topic">
    <option value="">Выберите тематику</option>
    <option value="business">Бизнес</option>
    <option value="technology">Технологии</option>
    <option value="advertising">Реклама и маркетинг</option>
  </select>
  <?= isset($errors['topic']) ? '<span style="color: red;">Не указана тема</span>' : '' ?>
  <br><br>

  <label for="payment_method">Метод оплаты:</label>
  <select id="payment_method" name="payment_method">
    <option value="">Выберите метод оплаты</option>
    <option value="webmoney">WebMoney</option>
    <option value="yandex_money">Яндекс.Деньги</option>
    <option value="paypal">PayPal</option>
    <option value="credit_card">Кредитная карта</option>
  </select>
  <?= isset($errors['payment']) ? '<span style="color: red;">Не указан метод оплаты</span>' : '' ?>
  <br><br>

  <input type="checkbox" id="subscribe" name="subscribe" checked>
  <label for="subscribe">Подписаться на рассылку о конференции</label><br><br>

  <input type="submit" value="Отправить заявку">
</form>
<?php
session_start();

$afk_time = 300;

if (!isset($_SESSION['authentification']) || $_SESSION['authentification'] === false) {
  header('Location: logout.php');
  exit();
}

if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > $afk_time)) {
  header('Location: logout.php');
  exit();
}

$_SESSION['last_activity'] = time();

require 'db_conn.php';

try {
    $stmt = $pdo->query("SELECT id, name, surname, email, phone, topic, payment_method, subscription, date, ip, status FROM participants");
    $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($_POST) {
        if (!empty($_POST['checked'])) {
            foreach ($_POST['checked'] as $key) {
                if (isset($items[$key])) {
                    $items[$key]['status'] = 'deleted';

                    $stmt = $pdo->prepare("UPDATE participants SET status = :status WHERE id = :id");
                    $stmt->execute([
                        'status' => $items[$key]['status'],
                        'id' => $items[$key]['id']
                    ]);
                }
            }
        }
    }

    include 'templates/admin_new.html';

} catch (PDOException $e) {
    die("Ошибка подключения к базе данных: " . $e->getMessage());
}
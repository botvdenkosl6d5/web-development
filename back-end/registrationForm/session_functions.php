<?php
error_reporting(0);
session_start();

function get_guest_info(string $filename) {
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $s_id = session_id();
    $_SESSION['hits_counter'] = 0;
    $file = $filename . '.txt';

    $lines = file($file, FILE_IGNORE_NEW_LINES);
    $found = false;
    foreach($lines as $key => $line){
        $arr = explode('|', $line);
        if(trim($arr[1]) === $ip_address && trim($arr[2]) === $s_id){
            $_SESSION['hits_counter'] = intval(trim($arr[3])) + 1;
            $lines[$key] = date('Y-m-d H:i:s') . '|' . $ip_address . '|' . $s_id . '|'. $_SESSION['hits_counter'];
            $found = true;
            break;
        }
    }

    if($found){
        $data = implode(PHP_EOL, $lines) . PHP_EOL;
        file_put_contents($file, $data, LOCK_EX);
    } else {
        $data = date('Y-m-d H:i:s') . '|' . $ip_address . '|' . $s_id . '|'. $_SESSION['hits_counter'] . PHP_EOL;
        file_put_contents($file, $data, FILE_APPEND | LOCK_EX);
    }
}

# Документация для проекта БД "Автосервис"

## Введение
Данная документация кратко описывает базу данных "Автосервис", предназначенную для управления клиентами, автомобилями, заказами, запчастями и сотрудниками автосервиса. База данных разработана для хранения и обработки информации о клиентах, их автомобилях, заказах, используемых запчастях и сотрудниках сервиса.

## Структура базы данных
База данных состоит из следующих таблиц:

1. **Таблица "Clients"** содержит информацию о клиентах автосервиса, включая их идентификатор, имя, фамилию, отчество, адрес и контактный телефон.
2. **Таблица "Cars"** содержит информацию о автомобилях клиентов, включая их идентификатор, марку, модель, год выпуска и связанный идентификатор клиента.
3. **Таблица "Orders"** содержит информацию о заказах на ремонт, включая их идентификатор, дату создания, стоимость, связанный идентификатор клиента, связанный идентификатор автомобиля и связанный идентификатор статуса заказа.
4. **Таблица "Order_Statuses"** содержит информацию о возможных статусах заказа, включая их идентификатор, название и текущий статус.
5. **Таблица "Order_Status_History"** содержит информацию о истории изменения статуса заказов, включая идентификатор записи, связанный идентификатор заказа, связанный идентификатор статуса и дату изменения.
6. **Таблица "Parts"** содержит информацию о запчастях, включая их идентификатор, название и цену.
7. **Таблица "Ordered_Parts"** содержит информацию о заказанных запчастях, включая их идентификатор, связанный идентификатор заказа, связанный идентификатор запчасти и количество.
8. **Таблица "Employees"** содержит информацию о сотрудниках автосервиса, включая их идентификатор, имя, должность и заработную плату.

## Связи между таблицами
Связи между таблицами в базе данных "Автосервис" определены следующим образом:

![scheme](image.png)

## Типовые операции и представления БД
Возможные операции, которые можно выполнять с базой данных, включают:

1. Добавление, обновление и удаление клиентов, автомобилей, заказов, запчастей и сотрудников.
2. Получение информации о клиентах, их автомобилях, заказах, запчастях и сотрудниках.
3. Поиск заказов по различным критериям, например, по клиенту, автомобилю или статусу.
4. Отслеживание истории изменений статуса заказов.

Эти операции описаны в `.sql` файле под комментариями "Представления" и "Типовые операции".

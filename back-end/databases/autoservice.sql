/*База данных "Автосервис"*/

/*Таблица с клиентами*/
CREATE TABLE Clients (
    id INT PRIMARY KEY,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    middle_name VARCHAR(255),
    address VARCHAR(255),
    phone VARCHAR(20)
);

INSERT INTO Clients (id, first_name, last_name, middle_name, address, phone)
VALUES
    (1, 'Ivan', 'Ivanov', 'Ivanovich', 'Lenin Street 6', '8 (924) 612-21-11'),
    (2, 'Sergey', 'Markov', 'Antonovich', 'Nevsky Prospect 12', '8 (962) 451-31-17'),
    (3, 'Yulia', 'Smirnova', 'Victorovna', 'Ulan Batorskya 2', '8 (924) 455 32-23');

/*таблица автомобилей клиентов*/
CREATE TABLE Cars (
    id INT PRIMARY KEY,
    manufacturer VARCHAR(50),
    model VARCHAR(50),
    year INT,
    client_id INT
);

INSERT INTO Cars (id, manufacturer, model, year, client_id)
VALUES
    (1, 'Toyota', 'Camry', 2007, 1),
    (2, 'Honda', 'Civic', 2020, 2),
    (3, 'Ford', 'Mustang', 2018, 3);

/*таблица заказов*/
CREATE TABLE Orders (
    id INT PRIMARY KEY,
    date_created DATE,
    cost DECIMAL(10,2),
    client_id INT,
    car_id INT,
    status_id INT
);

INSERT INTO Orders (id, date_created, cost, client_id, car_id)
VALUES
    (1, '2023-05-12', 25000.00, 1, 1),
    (2, '2023-04-28', 15513.00, 2, 2),
    (3, '2023-05-01', 100500.00, 3, 3);

/*таблица статуса заказов*/
CREATE TABLE Order_Statuses (
    id INT PRIMARY KEY,
    name VARCHAR(50),
    status VARCHAR(50)
);

INSERT INTO Order_Statuses (id, name, status)
VALUES
    (1, 'New', 'In Progress'),
    (2, 'Pending', 'On Hold'),
    (3, 'Completed', 'Delivered');

/*таблица истории статуса заказов*/
CREATE TABLE Order_Status_History (
    id INT PRIMARY KEY,
    order_id INT,
    status_id INT,
    date_changed DATE
);

INSERT INTO Order_Status_History (id, order_id, status_id, date_changed)
VALUES
    (4, 1, 1, '2023-05-15'),
    (5, 2, 2, '2023-05-21'),
    (6, 3, 3, '2023-05-26');

/*таблица запчастей*/
CREATE TABLE Parts (
    id INT PRIMARY KEY,
    name VARCHAR(100),
    price DECIMAL(10,2)
);

INSERT INTO Parts (id, name, price)
VALUES
    (1, 'K&N Oil Filter', 2500.99),
    (2, 'Greedy Turbocharger kit', 32500.99),
    (3, 'AEM Spark Plugs', 1750.99);

/*таблица заказанных запчастей*/
CREATE TABLE Ordered_Parts (
    id INT PRIMARY KEY,
    order_id INT,
    part_id INT,
    quantity INT
);

INSERT INTO Ordered_Parts (id, order_id, part_id, quantity)
VALUES
    (1, 1, 1, 2),
    (2, 1, 2, 1),
    (3, 2, 3, 4),
    (4, 3, 1, 1),
    (5, 3, 2, 2);

/*таблица работников*/
CREATE TABLE Employees (
    id INT PRIMARY KEY,
    name VARCHAR(255),
    position VARCHAR(50),
    salary DECIMAL(10,2),
    phone VARCHAR(20)
);

INSERT INTO Employees (id, name, position, salary, phone)
VALUES
    (1, 'Daniil Savanin', 'Mechanic', 55000.00, '8 (924) 619-32-15'),
    (2, 'Alexander Domnenko', 'Service Advisor', 60000.00, '8 (924) 325-73-31'),
    (3, 'Dmitry Savanin', 'Technician', 45000.00, '8 (964) 312-42-41');

/*ВЗАИМОСВЯЗИ ТАБЛИЦ*/

-- Связь cars с clients
ALTER TABLE Cars
ADD FOREIGN KEY (client_id) REFERENCES Clients(id)
ON DELETE SET NULL;

-- Связь orders с clients
ALTER TABLE Orders
ADD FOREIGN KEY (client_id) REFERENCES Clients(id)
ON DELETE SET NULL;

-- Связь orders с cars
ALTER TABLE Orders
ADD FOREIGN KEY (car_id) REFERENCES Cars(id)
ON DELETE SET NULL;

-- Связь ordered_parts с orders
ALTER TABLE Ordered_Parts
ADD FOREIGN KEY (order_id) REFERENCES Orders(id)
ON DELETE CASCADE;

-- Связь ordered_parts с parts
ALTER TABLE Ordered_Parts
ADD FOREIGN KEY (part_id) REFERENCES Parts(id)
ON DELETE CASCADE;

-- Связь order_status_history с orders
ALTER TABLE Order_Status_History
ADD FOREIGN KEY (order_id) REFERENCES Orders(id)
ON DELETE CASCADE;

-- Связь order_status_history с order_statuses
ALTER TABLE Order_Status_History
ADD FOREIGN KEY (status_id) REFERENCES Order_Statuses(id)
ON DELETE CASCADE;


/*ПРЕДСТАВЛЕНИЯ*/

/*информация о клиентах и их автомобилях*/
CREATE VIEW ClientCarsView AS
SELECT c.id AS client_id, c.first_name, c.last_name, c.phone, ca.id AS car_id, ca.manufacturer, ca.model, ca.year
FROM Clients c
JOIN Cars ca ON c.id = ca.client_id;

/*информация о заказах и их статусах*/
CREATE VIEW OrderStatusView AS
SELECT o.id AS order_id, o.date_created, o.cost, c.first_name, c.last_name, os.name AS status, os.status AS description
FROM Orders o
JOIN Clients c ON o.client_id = c.id
JOIN (
    SELECT osh.order_id, os.name, os.status
    FROM Order_Status_History osh
    JOIN Order_Statuses os ON osh.status_id = os.id
    WHERE osh.date_changed = (
        SELECT MAX(date_changed)
        FROM Order_Status_History
        WHERE order_id = osh.order_id
    )
) AS os ON o.id = os.order_id;

/*информация о заказах и использованных деталях в них*/
CREATE VIEW OrderPartsView AS
SELECT o.id AS order_id, o.date_created, c.first_name, c.last_name, p.name AS part_name, op.quantity
FROM Orders o
JOIN Clients c ON o.client_id = c.id
JOIN Ordered_Parts op ON o.id = op.order_id
JOIN Parts p ON op.part_id = p.id;

/*информация о работниках*/
CREATE VIEW EmployeesView AS
SELECT e.id AS employee_id, e.name, e.position, e.salary, e.phone
FROM Employees e;


/*ТИПОВЫЕ ОПЕРАЦИИ*/

/*Создание клиента*/
INSERT INTO Clients (id, first_name, last_name, middle_name, address, phone)
VALUES (4, 'Maria', 'Petrova', 'Ivanovna', 'Pushkin Street 10', '8 (911) 123-45-67');

/*Добавление автомобиля клиента*/
INSERT INTO Cars (id, manufacturer, model, year, client_id)
VALUES (4, 'Mazda', 'RX-7', 1993, 4);

/*Редактирование информации о клиентах*/
UPDATE Clients
SET address = 'Chaikovsky Street 5', phone = '8 (911) 987-65-43'
WHERE id = 1;

/*Удаление клиента*/
DELETE FROM Clients
WHERE id = 3;

/*Создание заказа*/
INSERT INTO Orders (id, date_created, cost, client_id, car_id, status_id)
VALUES (4, '2023-05-20', 15000.00, 2, 2, 1);

/*Добавление информации о заказанных запчастях*/
INSERT INTO Ordered_Parts (id, order_id, part_id, quantity)
VALUES (6, 4, 3, 3);

/*Обновление статуса заказа*/
UPDATE Orders
SET status_id = 3
WHERE id = 2;

/*Смена истории статуса заказа*/
INSERT INTO Order_Status_History (id, order_id, status_id, date_changed)
VALUES (7, 2, 3, '2023-05-25');
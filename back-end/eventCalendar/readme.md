## Описание
Небольшое и простое веб-приложение "Мой календарь" или же "Календарь событий". В нём 

Состоит из двух частей:
- Формы, где можно задать тему, тип, место, дата и время, а также длительность события.
- Список добавленных событий с деталями и фильтрами.

Также данное веб-приложение создаёт и использует свою SQL таблицу для хранения и редактирования добавленных событий.

![Пример 1](calendar1.png)
![Пример 2](calendar2.png)
![Пример 3](calendar3.png)
(() => {
    const todolist = document.querySelector('.todolist-container');
    const taskForm = document.querySelector('.details');
    const card = document.querySelector('.todo-form');
    const closeBttn = taskForm.querySelector('.cancel');
    const addBttn = todolist.querySelector('.apply-button');
    const taskItem = todolist.querySelector('.task-item');
    let taskInput = document.querySelector('.task-input');
    let prioCheck = taskForm.querySelector('.details-checkbox');
    

    let curAction = '';
    let parentNode;

    taskForm.addEventListener('submit', e => {
        e.preventDefault();
        if (curAction === 'create') {
            if (taskInput.value){
                let newItem = taskItem.cloneNode(true);
                let newItemPrio = newItem.querySelector('.task-item-prio')
                let taskItemText = newItem.querySelector('.task-item-descr');
                taskItemText.textContent = taskInput.value;
                changePrio(newItemPrio);
                todolist.append(newItem);
            }
            else {
                alert('Вы не ввели название задачи!');
            }
        }
        else if (curAction === 'edit') {
            let editedItem = parentNode.querySelector('.task-item-descr');
            let editedPrio = parentNode.querySelector('.task-item-prio');
            if (taskInput.value){
                changePrio(editedPrio);
                editedItem.textContent = taskInput.value;
            }
            else {
                alert('Вы не ввели название задачи!');
            }
        }
    })

    let changePrio = (item) => {
        prioCheck.checked? item.textContent = '⚡': item.textContent = '';
    }

    closeBttn.addEventListener('click', e => {
        card.classList.add('hidden');
    })
    addBttn.addEventListener('click', e => {
        card.classList.remove('hidden');
        curAction = 'create';
    })
    todolist.addEventListener('click', e => {
        if (e.target.dataset.action === 'delete') {
            parentNode = e.target.closest('.task-item');
            parentNode.remove();
        }
        else if(e.target.dataset.action === 'edit'){
            card.classList.remove('hidden');
            parentNode = e.target.closest('.task-item');
            curAction = 'edit';
        }

        else if(e.target.dataset.action === 'status'){
            let taskStatus = e.target.parentNode.querySelector('.task-item-descr');
            if (e.target.checked) {
                taskStatus.classList.add('done');
            }
            else {
                taskStatus.classList.remove('done');
            }
        }
    })
})();
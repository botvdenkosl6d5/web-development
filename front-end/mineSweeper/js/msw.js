let startGame = (width, height, bombs_count, flagCount) => {
    const field = document.querySelector('.msw');

    field.style['grid-template-columns'] = `repeat(${width}, 40px)`;
    field.style.setProperty('--columns-count', width);

    const cellsCount = width * height;

    for (let i = 0; i < cellsCount; i++) {
        let newCell = document.createElement('div');
        newCell.classList.add('msw-cell');
        newCell.classList.add('closed');
        field.append(newCell);
    }

    const cells = [...field.children];
    const bombs = [...Array(cellsCount).keys()].sort(() => Math.random() - 0.5).slice(0, bombs_count);
    let closedCount = cellsCount;
    field.addEventListener('click', (e) => {
        if (!e.target.classList.contains('msw-cell')) {
            return;
        }
        const index = cells.indexOf(e.target);
        const col = index % width;
        const row = Math.floor(index / width);
        openCell(row, col);
    })
    field.addEventListener('contextmenu', (e) => {
        e.preventDefault();
        if (!e.target.classList.contains('msw-cell')) {
            return;
        }
        if (flagCount > 0) {
            e.target.classList.remove('closed');
            e.target.classList.add('mark');
            flagCount--;
            console.log(flagCount);
        }
        else {
            alert('У вас закончились флажки');
        }
    })

    let openCell = (row, column) => {
        if (!checkValidCell(row, column)) return;

        const index = row * width + column;
        const cell = cells[index];
        if (!cell.classList.contains('closed')) return;
        cell.classList.remove('closed');

        if (isBomb(row, column)) {
            cell.classList.add('bomb');
            setTimeout(() => {
                alert('Вы проиграли');
                document.location.reload();
            }, 100);
            return;
        }
        closedCount--;
        console.log(closedCount);
        if (closedCount <= bombs_count) {
            setTimeout(() => {
                alert('Вы выиграли!');
                document.location.reload();
            }, 100);
        }

        let minesCount = getMinesCount(row, column);

        if (minesCount !== 0) {
            cell.innerHTML = minesCount;
            return;
        }

        for (let i = -1; i <= 1; i++){
            for (let j = -1; j <= 1; j++){
                openCell(row + i, column + j);
            }
        }
    }

    let getMinesCount = (row, column) => {
        let count = 0;
        for (let i = -1; i <= 1; i++) {
            for (let j = - 1; j <= 1; j++) {
                if (isBomb(row + j, column + i)) {
                    count++;
                }
            }
        }
        return count;
    }
    let checkValidCell = (row, column) => {
        return row >= 0 && row < height && column >= 0 && column < width;
    }

    let isBomb = (row, col) => {
        if (!checkValidCell(row, col)) return false;
        const index = row * width + col;
        return bombs.includes(index);
    }
}
startGame(12, 12, 8, 4);
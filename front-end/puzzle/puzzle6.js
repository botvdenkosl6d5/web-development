document.addEventListener('DOMContentLoaded', () => {

    const width = 9;
    const height = 6;
    let pieceLeft = width * height;
    const size = 50;
    const imageUrl = './img/nero.jpg'

    game = document.querySelector('.puzzle-game');
    let puzzleArea = document.getElementById('puzzle-area');
    let result = document.getElementById('puzzle-result');

    puzzleArea.style.gridTemplateColumns = `repeat(${width}, ${size}px)`;
    result.style.gridTemplateColumns = `repeat(${width}, ${size}px)`;
    let tiles = [];
    for (let y = 0; y < height; y++) {
        tiles[y] = [];
        for (let x = 0; x < width; x++) {
            let cell = document.createElement('div');
            cell.className = 'puzzle-cell';
            cell.x = x;
            cell.y = y;
            cell.style.width = `${size}px`;
            cell.style.height = `${size}px`;
            result.append(cell);

            let tile = document.createElement('div');
            tile.x = x;
            tile.y = y;
            tile.className = 'puzzle-item';
            tile.style.backgroundImage = 'url(' + imageUrl + ')';
            tile.style.width = `${size}px`;
            tile.style.height = `${size}px`;
            tile.style.backgroundPosition = `${-x * size}px ${-y * size}px`;
            tile.style.backgroundRepeat = 'no-repeat';
            puzzleArea.append(tile);
            tiles[y].push(tile);
        }
    }

    let leftPane = game.querySelector('.left-pane');
    let srcWidth = leftPane.offsetWidth;
    let srcHeight = leftPane.offsetHeight;

    for (let i = 0; i < 1000; i++) {
        let randX = Math.round(Math.random() * (width - 1));
        let randY = Math.round(Math.random() * (height - 1));
        let randTile = tiles[randY][randX];

        let posLeft = Math.round(Math.random() * (srcWidth - size * 2));
        let posTop = Math.round(Math.random() * (srcHeight - size * 2));

        randTile.style.position = 'absolute';
        randTile.style.left = `${posLeft}px`;
        randTile.style.top = `${posTop}px`;
        puzzleArea.append(randTile);
    }

    puzzleArea.addEventListener('mousedown', e => {
        let target = e.target;
        if (target.classList.contains('puzzle-item')) {
            e.preventDefault();
            for (let tile of puzzleArea.children) {
                tile.style.zIndex = 0;
            }
            target.style.zIndex = 10;
            target.style.pointerEvents = 'none';

            let initX = e.clientX;
            let initY = e.clientY;

            let checkMove = e => {
                let diffX = e.clientX - initX;
                let diffY = e.clientY - initY;
                target.style.top = `${target.offsetTop + diffY}px`;
                target.style.left = `${target.offsetLeft + diffX}px`;
                initX = e.clientX;
                initY = e.clientY;
            };

            let checkUp = e => {
                target.style.pointerEvents = 'all';
                document.removeEventListener('mousemove', checkMove);
                document.removeEventListener('mouseup', checkUp);
                result.removeEventListener('mouseup', checkOver);
            };

            document.addEventListener('mousemove', checkMove);
            document.addEventListener('mouseup', checkUp);

            let checkOver = e => {
                let t = e.target;
                if (t.classList.contains('puzzle-cell')) {
                    if (t.x === target.x && t.y === target.y) {
                        target.style.position = 'relative';
                        target.style.top = 0;
                        target.style.left = 0;
                        target.style.zIndex = 0;
                        t.append(target);
                        pieceLeft--;
                        if (pieceLeft === 0) {
                            alert('Вы собрали пазл');
                        }
                    }
                }
            };
            result.addEventListener('mouseup', checkOver);
        }
    })
})
(function(){
    window.drawCard = function(form) {
        let resultDiv = form.closest('.card-container').querySelector('.card-result > .card');

        let fioAlign = form['customize'];
        let fioSize = form['customize-size'];
        let posAlign = form['customize-position'];
        let posSize = form['customize-position-size'];
        let showContent = form['check'];

        let fields = [
            'organization',
            'fio',
            'position',
            'number',
            'email',
            'adress'
        ];

        fields.forEach(field => {
            resultDiv.querySelector('.card-' + field).textContent = form[field].value;
        })

        for (let i = 0; i <= 2; i++){
            if (fioAlign[i].checked){
                resultDiv.querySelector('.card-fio').style.textAlign = fioAlign[i].value;
            }
            
            if (fioSize[i].checked){
                resultDiv.querySelector('.card-fio').style.font = fioSize[i].value;
            }

            if (posAlign[i].checked){
                resultDiv.querySelector('.card-position').style.textAlign = posAlign[i].value;
            }

            if (posSize[i].checked){
                resultDiv.querySelector('.card-position').style.font = posSize[i].value;
            }
        }

        if (!showContent[0].checked){
            resultDiv.querySelector('.card-email').style.display = "none"
        }
        else {
            resultDiv.querySelector('.card-email').style.display = "block"
        }

        if (!showContent[1].checked){
            resultDiv.querySelector('.card-adress').style.display = "none"
        }
        else{
            resultDiv.querySelector('.card-adress').style.display = "block"
        }
    }
})();